package com.flights.entities;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

public class Route {

    private String id;
    private String flyFrom;
    private String flyTo;
    private String airline;
    private Long aTime;
    private Long dTime;
    private Long aTimeUTC;
    private Long dTimeUTC;

    public String getId() {
        return id;
    }

    public String getFlyFrom() {
        return flyFrom;
    }

    public String getFlyTo() {
        return flyTo;
    }

    public String getAirline() {
        return airline;
    }

    public Long getaTime() {
        return aTime;
    }

    public Long getaTimeUtc() {
        return aTimeUTC;
    }

    public Long getdTime() {
        return dTime;
    }

    public Long getdTimeUtc() {
        return dTimeUTC;
    }

    public String getFlyFromFlyToCodes() {
        StringBuilder flyFromFlyTo = new StringBuilder();
        flyFromFlyTo.append(flyFrom);
        flyFromFlyTo.append(" - ");
        flyFromFlyTo.append(flyTo);
        return flyFromFlyTo.toString();
    }
}
