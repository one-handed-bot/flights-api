package com.flights.entities;

import java.util.HashMap;
import java.util.TreeMap;

public class FlightsInfo {

    private String search_id;
    private FlightData[] data;

    public String getSearchId() {
        return search_id;
    }

    public FlightData[] getFlightData(){
        return data;
    }

    public HashMap<String, Object> getFlightsStats(){
        double bag1Sum = 0;
        double bag2Sum = 0;
        double flightSum = 0;
        double bag1Avg = 0;
        double bag2Avg = 0;
        double flightAvg = 0;
        int totalFlights = data.length;
        TreeMap <Integer, TreeMap<String, Object>> routesDetails = new TreeMap<>();

        for (int i = 0; i < totalFlights; i++) {
            bag1Sum += data[i].getBagPrice("1");
            bag2Sum += data[i].getBagPrice("2");

            flightSum += data[i].getConversionInCurrency("EUR");

            routesDetails.put(i, data[i].getRoutesDetails());
        }

        HashMap<String, Object> flightsStats = new HashMap<>();
        flightsStats.put("bag1Sum", bag1Sum);
        flightsStats.put("bag2Sum", bag2Sum);
        flightsStats.put("flightSum", flightSum);
        flightsStats.put("totalFlights", totalFlights);
        flightsStats.put("routes", routesDetails);

        try {
            bag1Avg = bag1Sum / totalFlights;
            bag2Avg = bag2Sum / totalFlights;
            flightAvg = flightSum / totalFlights;
        } catch (ArithmeticException e) {
            System.out.println("ArithmeticException in averages calculation!");
            e.printStackTrace();
        }

        flightsStats.put("bag1Avg", bag1Avg);
        flightsStats.put("bag2Avg", bag2Avg);
        flightsStats.put("flightAvg", flightAvg);

        return flightsStats;
    }

}
