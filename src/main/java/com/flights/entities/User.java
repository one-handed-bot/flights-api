package com.flights.entities;

import com.google.api.client.json.GenericJson;
import com.google.api.client.util.Key;

public class User extends GenericJson {

    @Key
    private String login;
    @Key
    private long id;
    @Key("email")
    private String email;

}
