package com.flights.entities;

import java.util.HashMap;
import java.util.TreeMap;

public class FlightsStat {

    private String currency;
    private int total_flights;
    private double bag1_avg;
    private double bag2_avg;
    private double flight_avg;
    private TreeMap<Integer, TreeMap<String, Long>> routes;

    public FlightsStat(HashMap<String, Object> mappedFlightStats) {
        setCurrency("EUR");

        setTotalFlights((Integer) mappedFlightStats.getOrDefault("totalFlights", 1));

        setBag1Avg((Double) mappedFlightStats.getOrDefault("bag1Avg",0));
        setBag2Avg((Double) mappedFlightStats.getOrDefault("bag2Avg",0));
        setFlightAvg((Double) mappedFlightStats.getOrDefault("flightAvg",0));

        setRoutes((TreeMap<Integer, TreeMap<String,Long>>) mappedFlightStats.get("routes"));
    }

    public String getCurrency() {
        return currency;
    }

    public int getTotalFlights() {
        return total_flights;
    }

    public double getBag1Avg() {
        return bag1_avg;
    }

    public double getBag2Avg() {
        return bag2_avg;
    }

    public double getFlightAvg() {
        return flight_avg;
    }

    public TreeMap<Integer, TreeMap<String, Long>> getRoutes() {
        return routes;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public void setTotalFlights(int totalFlights) {
        this.total_flights = totalFlights;
    }

    public void setBag1Avg(double bag1Avg) {
        this.bag1_avg = bag1Avg;
    }

    public void setBag2Avg(double bag2Avg) {
        this.bag2_avg = bag2Avg;
    }

    public void setFlightAvg(double flightAvg) {
        this.flight_avg = flightAvg;
    }

    public void setRoutes(TreeMap<Integer, TreeMap<String, Long>> routes) {
        this.routes = routes;
    }
}
