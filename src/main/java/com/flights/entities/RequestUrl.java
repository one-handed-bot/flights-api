package com.flights.entities;

import com.google.api.client.http.GenericUrl;

public class RequestUrl extends GenericUrl {

    public RequestUrl(String encodedUrl) {
        super(encodedUrl);
    }

}