package com.flights.entities;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

public class FlightData {

    private String id;
    private String flyFrom;
    private String flyTo;
    private Map<String, Double> bags_price;
    private Route[] route;
    private HashMap<String,Double> conversion;

    public String getId() {
        return id;
    }

    public String getFlyFrom() {
        return flyFrom;
    }

    public String getFlyTo() {
        return flyTo;
    }

    public Map<String, Double> getBagsPrice() {
        return bags_price;
    }

    public Double getBagPrice(String bagNumber){
        double bagPrice = (bags_price.get(bagNumber) == null) ? 0 : bags_price.get(bagNumber);
        return bagPrice;
    }

    public HashMap<String, Double> getConversion() {
        return conversion;
    }

    public Double getConversionInCurrency(String currency){
        return conversion.get(currency);
    }

    public TreeMap<String, Object> getRoutesDetails(){
        long numberOfStops = route.length;
        long firstDepartureDate = 32233075200L;
        long finalArrivalDate = 0L;
        String fromToAirports = "";
        StringBuilder airlineCodesBuilder = new StringBuilder();
        String airlineCodes = "";

        for (int i = 0; i < numberOfStops; i++) {
            long departureDate = route[i].getdTimeUtc();
            long arrivalDate = route[i].getaTimeUtc();

            firstDepartureDate = (firstDepartureDate <= departureDate) ? firstDepartureDate : departureDate;
            finalArrivalDate = (finalArrivalDate >= arrivalDate) ? finalArrivalDate : arrivalDate;
            fromToAirports = route[i].getFlyFromFlyToCodes();

            airlineCodes = buildAirlineCodes(airlineCodesBuilder, route[i], i, numberOfStops);
        }

        TreeMap<String, Object> routeDetails = new TreeMap<>();
        routeDetails.put("number_of_stops", numberOfStops - 1);
        routeDetails.put("date_from", convertDates(firstDepartureDate));
        routeDetails.put("date_to", convertDates(finalArrivalDate));
        routeDetails.put("from_to", fromToAirports);
        routeDetails.put("airline_codes", airlineCodes);

        return routeDetails;
    }

    private String buildAirlineCodes(StringBuilder builder, Route route, int i, long length) {
        builder.append(route.getAirline());
        if (i+1 < length) {
            builder.append(" - ");
        }
        return builder.toString();
    }

    private String convertDates(Long dateToFormat){
        int secondsToMilliseconds = 1000;
        return new SimpleDateFormat("MM/dd/yyyy KK:mm:ss a Z").format(dateToFormat * secondsToMilliseconds);
    }

}
