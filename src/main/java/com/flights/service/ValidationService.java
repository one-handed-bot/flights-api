package com.flights.service;

import com.flights.helpers.FormattedDateMatcher;

import java.security.InvalidParameterException;
import java.util.Map;

public class ValidationService {

    public enum AIRPORT_CODE {
        PORTO("OPO"),
        LISBON("LIS");

        private final String airportCode;

        AIRPORT_CODE(String airportCode){
            this.airportCode = airportCode;
        }

        public String getAirportCode(){ return this.airportCode; }
    }

    public void validateRequestParams (Map<String, String> params) throws InvalidParameterException {
        Boolean isValid = false;

        String dest = params.get("dest");
        String from = params.get("from");
        isValid = validateAirports(from, dest);

        if (!isValid) throw new InvalidParameterException("Airport Codes are invalid!");

        FormattedDateMatcher matcher = new FormattedDateMatcher();
        String dateFrom = params.get("dateFrom");
        String dateTo = params.get("dateTo");
        isValid &= matcher.matches(dateFrom);
        isValid &= matcher.matches(dateTo);

        if (!isValid) throw new InvalidParameterException("Date format must be yyyy/mm/dd !");

        System.out.println("validateRequestParams: all accepted params are valid");
    }

    private Boolean validateAirports(String from, String dest) {
        Boolean isValid = false;

        isValid |= validateAirportCode(from);
        isValid &= validateAirportCode(dest);
        // Can't have from-to airports to be the same
        isValid &= !from.equals(dest);

        return isValid;
    }

    private Boolean validateAirportCode(String wordToCompare){
        Boolean isIncluded = false;

        isIncluded |= AIRPORT_CODE.PORTO.getAirportCode().equals(wordToCompare);
        isIncluded |= AIRPORT_CODE.LISBON.getAirportCode().equals(wordToCompare);

        return isIncluded;
    }
}
