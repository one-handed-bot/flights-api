package com.flights.service;

import com.flights.entities.RequestUrl;
import com.google.api.client.http.*;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.JsonObjectParser;
import com.google.api.client.json.jackson2.JacksonFactory;
import java.io.*;
import java.util.HashMap;

class FlightClient {

    static HttpTransport HTTP_TRANSPORT = new NetHttpTransport();
    static JsonFactory JSON_FACTORY = new JacksonFactory();
    
    String makeRequest(HashMap<String, String> queryParams) throws Exception {
        String response = "";
        HttpRequestFactory requestFactory = HTTP_TRANSPORT.createRequestFactory();

        String url = buildUrl(queryParams);

        RequestUrl requestUrl = new RequestUrl(url);
        HttpRequest request = requestFactory.buildGetRequest(requestUrl);
        setHeaders(request);
        request.setConnectTimeout(3 * 60000);  // 3 minutes connect timeout
        request.setReadTimeout(3 * 60000);  // 3 minutes read timeout

        long startTime = System.currentTimeMillis();
        InputStream initialStream = request.execute().getContent();
        System.out.println("Executed external api call in: " + (System.currentTimeMillis() - startTime) + " milliseconds");

        response = convertResponse(initialStream);
        initialStream.close();

        return response;
    }

    private void setHeaders(HttpRequest request){
        // Identify content type of the intended response
        HttpHeaders headers = request.getHeaders();
        // receive JSON content only
        headers.setContentType("application/json");
        request.setParser(new JsonObjectParser(JSON_FACTORY));
    }

    private String convertResponse(InputStream inputStream) throws IOException {
        InputStreamReader isReader = new InputStreamReader(inputStream);
        BufferedReader reader = new BufferedReader(isReader);
        StringBuffer bufferedResult = new StringBuffer();
        String line;
        while((line = reader.readLine())!= null){
            bufferedResult.append(line);
        }

        return bufferedResult.toString();
    }

    private String buildUrl(HashMap<String,String> queryParams){
        String flyFrom = queryParams.get("from");
        String flyTo = queryParams.get("dest");
        String dateFrom = queryParams.get("dateFrom");
        String dateTo = queryParams.get("dateTo");

        StringBuilder urlBuilder = new StringBuilder();
        urlBuilder.append("https://api.skypicker.com/flights?select_airlines=TP,FR&partner=picky&curr=EUR");

        urlBuilder.append("&flyFrom=").append(flyFrom);
        urlBuilder.append("&to=").append(flyTo);
        urlBuilder.append("&date_from=").append(dateFrom);
        urlBuilder.append("&date_to=").append(dateTo);

        System.out.println("url is: " + urlBuilder.toString());

        return urlBuilder.toString();
    }

}


