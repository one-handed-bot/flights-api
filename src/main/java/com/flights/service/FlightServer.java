package com.flights.service;

import com.flights.entities.FlightsInfo;
import com.flights.entities.FlightsStat;
import com.flights.helpers.ApiError;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sun.net.httpserver.*;
import org.apache.http.HttpStatus;

import java.io.*;
import java.net.InetSocketAddress;
import java.net.URI;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.LinkedHashMap;

public class FlightServer {

    public FlightServer() {
        initServer();
    }

    private void initServer() {
        HttpServer server = null;
        try {
            server = HttpServer.create(new InetSocketAddress(8080), 0);
        } catch (IOException e) {
            e.printStackTrace();
        }
        HttpContext context = server.createContext("/flights-api/get-average");

        context.setHandler((exchange) -> {
            try {
                handleRequest(exchange);
            } catch (Exception e) {
                System.out.println("Failed to handle request");
                e.printStackTrace();
                ApiError apiError = new ApiError(HttpStatus.SC_INTERNAL_SERVER_ERROR, e.getMessage());
                sendResponse(exchange, apiError.getMessage(), apiError.getStatusCode());
            }
        });

        server.start();
    }

    private static void handleRequest(HttpExchange exchange) throws Exception {
        HashMap<String, String> queryParams = getQueryParams(exchange);

        FlightClient flightClient = new FlightClient();
        String result = flightClient.makeRequest(queryParams);

        Gson gson = new Gson();

        if ("true".equals(queryParams.get("prettyMode"))){
            gson = new GsonBuilder().setPrettyPrinting().create();
        }

        FlightsInfo flightsInfo = gson.fromJson(result, FlightsInfo.class);
        HashMap<String, Object> mappedFlightStats = flightsInfo.getFlightsStats();
        FlightsStat flightsStat = new FlightsStat(mappedFlightStats);
        String showResponse = gson.toJson(flightsStat);

        try {
            sendResponse(exchange, showResponse, HttpStatus.SC_OK);
        } catch (IOException e) {
            e.printStackTrace();
            throw new Exception("IOException in sendResponse method");
        }
    }

    private static HashMap<String, String> getQueryParams(HttpExchange exchange) throws Exception {
        System.out.println("-- query --");
        URI requestURI = exchange.getRequestURI();
        String query = requestURI.getQuery();
        System.out.println(query);

        HashMap<String, String> queryParams = splitQuery(query);
        ValidationService validationService = new ValidationService();
        validationService.validateRequestParams(queryParams);

        return queryParams;
    }

    private static HashMap<String, String> splitQuery(String query) throws UnsupportedEncodingException {
        HashMap<String, String> queryParams = new LinkedHashMap<String, String>();
        String[] pairs = query.split("&");
        // TODO: store values in Map<String, List<String>> if parameters can be repeated
        for (String pair : pairs) {
            int idx = pair.indexOf("=");
            queryParams.put(URLDecoder.decode(pair.substring(0, idx), "UTF-8"),
                    URLDecoder.decode(pair.substring(idx + 1), "UTF-8")
            );
        }
        return queryParams;
    }

    private static void sendResponse(HttpExchange exchange, String showResponse, int statusCode) throws IOException {
        String encoding = "UTF-8";
        byte[] responseBytes = showResponse.getBytes(encoding);

        // send JSON content only
        exchange.getResponseHeaders().set("Content-Type", "application/json; charset=" + encoding);
        exchange.sendResponseHeaders(statusCode, responseBytes.length);

        OutputStream os = exchange.getResponseBody();
        os.write(responseBytes);

        os.flush();
        os.close();
    }

}
