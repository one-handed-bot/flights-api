package com.flights.helpers;

public interface DateMatcher {
    boolean matches(String date);
}