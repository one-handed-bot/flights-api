package com.flights.helpers;

import org.apache.http.HttpStatus;

import java.util.Arrays;
import java.util.List;

public class ApiError {

    private int statusCode;
    private String message;

    public ApiError(int statusCode, String message) {
        super();
        this.statusCode = statusCode;
        this.message = message;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public String getMessage() {
        return message;
    }

}
