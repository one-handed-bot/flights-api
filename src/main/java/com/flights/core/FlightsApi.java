package com.flights.core;

import com.flights.service.FlightServer;

public class FlightsApi
{
    public static void main( String[] args )
    {
        System.out.println( "-------- Starting FlightsApi app --------" );

        try {
            FlightServer flightServer = new FlightServer();
            System.out.println( "-------- Running FlightsApi app --------" );
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println( "-------- Stopped FlightsApi app --------" );
        }

    }
}
