#!/bin/bash

# description: Flights API Server Daemon Start, Stop and Restart

# Source function library.
#. /etc/rc.d/init.d/functions

jar_dir=target

memory_opt="-Xms1g -Xmx2g"
gc_params="-XX:+UseConcMarkSweepGC -XX:+UseParNewGC -Djava.net.preferIPv4Stack=true -XX:+CMSIncrementalPacing -XX:ParallelGCThreads=2 -XX:+AggressiveOpts"


start_serv() {
    echo "[copy and execute] => java -jar "$memory_opt" "$gc_params" -Dfile.encoding=UTF-8 "$jar_dir"/flights-api.jar"
}

stop_serv() {
    echo "[NA] - to implement"
}

restart_serv() {
    echo "[NA] - to implement"
}

case "$1" in
    start)   start_serv ;;
    stop)    stop_serv ;;
    restart) restart_serv ;;
    *) echo "usage: $0 start|stop|restart" >&2
       exit 1
       ;;
esac