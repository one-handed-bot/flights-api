# flights-api
Using Kiwi API to summarize flight's statistics

## Setup

1 - Execute `mvn package` to create the .jar file

2 - In the root of the project, execute ./bin/flights-api start

3 - Follow the instructions (copy and execute the command)

## Usage

#### Endpoint: 'flights-api/get-average'

Available Parameters are:
- from (OPO|LIS)
- dest (OPO|LIS)
- dateFrom (format dd/mm/yyyy)
- dateTo (format dd/mm/yyyy)
- prettyMode (prettify json response)

Example: http://localhost:8080/flights-api/get-average?dest=OPO&from=LIS&dateFrom=08/11/2019&dateTo=08/11/2019&prettyMode=false


### Troubleshooting

- if you get the error `java.lang.NoClassDefFoundError: com/google/api/client/http/HttpTransport`,
please go to `com/flights/core/FlightsApi.java` and run manually.
